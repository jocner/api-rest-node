'use strict'

var express = require('express');
var ArticleController = require('../controllers/article');

var router = express.Router();

router.post('/save-article', ArticleController.save);
router.get('/articles', ArticleController.getArticle);
router.delete('/delete-article/:id', ArticleController.deleteArticle);

module.exports = router;