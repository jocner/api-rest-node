'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var ArticleSchema = Schema({
    name: String,
    description: String,
    time: Date
});

module.exports = mongoose.model('Article', ArticleSchema);